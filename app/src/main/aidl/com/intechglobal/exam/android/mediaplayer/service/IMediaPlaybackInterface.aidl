// IMediaPlaybackInterface.aidl
package com.intechglobal.exam.android.mediaplayer.service;

// Declare any non-default types here with import statements

interface IMediaPlaybackInterface
{
    void openFile(String path);
    boolean isPlaying();
    boolean isPaused();
    void stop();
    void pause();
    void play();
    long getDuration();
    long getPosition();
    long seek(long pos);
}
