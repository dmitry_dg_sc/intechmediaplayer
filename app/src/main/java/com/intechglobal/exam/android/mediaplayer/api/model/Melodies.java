package com.intechglobal.exam.android.mediaplayer.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DG on 18.02.2016.
 * @see Melody
 */
public class Melodies implements Parcelable
{
    private List<Melody> melodies;

    public Melodies()
    {

    }

    protected Melodies(Parcel in)
    {
        melodies = in.createTypedArrayList(Melody.CREATOR);
    }

    public static final Creator<Melodies> CREATOR = new Creator<Melodies>()
    {
        @Override
        public Melodies createFromParcel(Parcel in)
        {
            return new Melodies(in);
        }

        @Override
        public Melodies[] newArray(int size)
        {
            return new Melodies[size];
        }
    };

    public List<Melody> getMelodies()
    {
        return melodies;
    }

    public void setMelodies(List<Melody> melodies)
    {
        this.melodies = melodies;
    }

    /**
     * Adds from another source
     * @param melodies
     */
    public boolean add(Melodies melodies)
    {
        return add(melodies.getMelodies());
    }

    /**
     * Adds a list of melody items
     * @param list
     */
    public boolean add(List<Melody> list)
    {
        // create if not exists array of melodies
        ensureExists();

        return melodies.addAll(list);
    }

    /**
     * Adds a single melody item
     * @param melody
     * @return
     */
    public boolean add(Melody melody)
    {
        // create if not exists array of melodies
        ensureExists();

        return melodies.add(melody);
    }

    public int size()
    {
        return melodies != null ? melodies.size() : 0;
    }

    public Melody get(int position)
    {
        if(position >= 0 && position < size())
        {
            return melodies.get(position);
        }
        return null;
    }

    private void ensureExists()
    {
        if(melodies == null)
        {
            melodies = new ArrayList<>();
        }
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeTypedList(melodies);
    }
}
