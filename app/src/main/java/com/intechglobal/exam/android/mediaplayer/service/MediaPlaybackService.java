package com.intechglobal.exam.android.mediaplayer.service;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.intechglobal.exam.android.mediaplayer.BuildConfig;
import com.intechglobal.exam.android.mediaplayer.media.Consts;
import com.intechglobal.exam.android.mediaplayer.media.InternalMediaPlayer;

import java.lang.ref.WeakReference;

/**
 * Created by DG on 19.02.2016.
 */
public class MediaPlaybackService extends Service
{
    public static final String TAG = MediaPlaybackService.class.getSimpleName();

    private final IBinder mBinder = new ServiceStub(this);

    private int                 mServiceStartId;
    private InternalMediaPlayer mMediaPlayer;
    private boolean             mIsPlaying;
    private boolean             mIsPaused;

    public MediaPlaybackService()
    {

    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        mMediaPlayer = new InternalMediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener()
        {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra)
            {

                Intent intent = new Intent(Consts.ACTION_PLAYER_ERROR);
                intent.putExtra(Consts.ARG_ERROR_WHAT, what);
                intent.putExtra(Consts.ARG_ERROR_EXTRA, extra);

                //ComponentName resolveInfo = intent.resolveActivity(getActivity().getPackageManager());
                LocalBroadcastManager.getInstance(MediaPlaybackService.this).sendBroadcast(intent);

                return false;
            }
        });
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mp)
            {

                Intent intent = new Intent(Consts.ACTION_PLAYER_STATE);
                intent.putExtra(Consts.ARG_STATE_KEY, mMediaPlayer.isPrepared() ? Consts.ARG_STATE_PREPARED : Consts.ARG_STATE_UNKNOWN);
                LocalBroadcastManager.getInstance(MediaPlaybackService.this).sendBroadcast(intent);
            }
        });
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                Intent intent = new Intent(Consts.ACTION_PLAYER_STATE);
                intent.putExtra(Consts.ARG_STATE_KEY, Consts.ARG_STATE_COMPLETE);
                LocalBroadcastManager.getInstance(MediaPlaybackService.this).sendBroadcast(intent);
            }
        });
    }

    @Override
    public void onDestroy()
    {
        mMediaPlayer.release();
        mMediaPlayer = null;

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    @Override
    public void onRebind(Intent intent)
    {

    }

    @Override
    public boolean onUnbind(Intent intent)
    {
        // stop the service right now
        stopSelf(mServiceStartId);
        return true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        mServiceStartId = startId;

        return START_STICKY;
    }

    private void openFile(String path)
    {
        mMediaPlayer.setDataSource(path);
    }

    private boolean isPlaying() throws RemoteException
    {
        return mIsPlaying;
    }

    private boolean isPaused() throws RemoteException
    {
        return mIsPaused;
    }

    private void stop() throws RemoteException
    {
        mMediaPlayer.stop();
        mIsPlaying = false;
        mIsPaused = false;

        Intent intent = new Intent(Consts.ACTION_PLAYER_STATE);
        intent.putExtra(Consts.ARG_STATE_KEY, Consts.ARG_STATE_STOPPED);
        LocalBroadcastManager.getInstance(MediaPlaybackService.this).sendBroadcast(intent);
    }

    private void pause() throws RemoteException
    {
        mMediaPlayer.pause();
        mIsPlaying = false;
        mIsPaused = true;

        Intent intent = new Intent(Consts.ACTION_PLAYER_STATE);
        intent.putExtra(Consts.ARG_STATE_KEY, Consts.ARG_STATE_PAUSED);
        LocalBroadcastManager.getInstance(MediaPlaybackService.this).sendBroadcast(intent);
    }

    private void play() throws RemoteException
    {
        if(mMediaPlayer.isPrepared())
        {
            mMediaPlayer.setVolume(1, 1);
            mMediaPlayer.start();

            mIsPlaying = true;
            mIsPaused = false;

            Intent intent = new Intent(Consts.ACTION_PLAYER_STATE);
            intent.putExtra(Consts.ARG_STATE_KEY, Consts.ARG_STATE_PLAY);
            LocalBroadcastManager.getInstance(MediaPlaybackService.this).sendBroadcast(intent);
        }
    }

    /**
     * Returns the duration of the file in milliseconds.
     * Currently this method returns -1 for the duration of MIDI files.
     */
    private long getDuration()
    {
        if(mMediaPlayer.isPrepared())
        {
            return mMediaPlayer.getDuration();
        }
        return 0;
    }

    /**
     * Returns the current playback position in milliseconds
     */
    private long getPosition()
    {
        if(mMediaPlayer.isPrepared())
        {
            return mMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    private long seek(long msec)
    {
        mMediaPlayer.seekTo((int) msec);
        return msec;
    }

    static class ServiceStub extends IMediaPlaybackInterface.Stub
    {
        WeakReference<MediaPlaybackService> mService;

        ServiceStub(MediaPlaybackService service)
        {
            mService = new WeakReference<>(service);
        }

        @Override
        public void openFile(String path) throws RemoteException
        {
            mService.get().openFile(path);
        }

        @Override
        public boolean isPlaying() throws RemoteException
        {
            return mService.get().isPlaying();
        }

        @Override
        public boolean isPaused() throws RemoteException
        {
            return mService.get().isPaused();
        }

        @Override
        public void stop() throws RemoteException
        {
            mService.get().stop();
        }

        @Override
        public void pause() throws RemoteException
        {
            mService.get().pause();
        }

        @Override
        public void play() throws RemoteException
        {
            mService.get().play();
        }

        @Override
        public long getDuration() throws RemoteException
        {
            return mService.get().getDuration();
        }

        @Override
        public long getPosition() throws RemoteException
        {
            return mService.get().getPosition();
        }

        @Override
        public long seek(long pos) throws RemoteException
        {
            return mService.get().seek(pos);
        }
    }
}
