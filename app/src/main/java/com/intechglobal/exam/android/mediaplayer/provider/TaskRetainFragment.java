package com.intechglobal.exam.android.mediaplayer.provider;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.intechglobal.exam.android.mediaplayer.api.Consts;
import com.intechglobal.exam.android.mediaplayer.api.IntechMelodyApi;
import com.intechglobal.exam.android.mediaplayer.api.model.Melodies;
import com.intechglobal.exam.android.mediaplayer.api.model.Melody;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DG on 19.02.2016.
 */
public class TaskRetainFragment extends Fragment implements IDataHelper.IDataProvider, Callback<Melodies>
{
    public static final  String  TAG       = TaskRetainFragment.class.getSimpleName();

    private static final int NO_RESULTS_YET = -1;

    /* last answer size.
     if empty array returned {"melodies":[]} mLastResultSize == 0
     if no results yet got mLastResultSize == -1
     we use it because api have not a range fields in response
       */
    private int      mLastResultSize;
    /* stored parsed response */
    private Melodies mMelodies;

    private IDataHelper.TaskCallbacks mTaskCallbacks;
    private boolean                   mRunning;
    // requested offset
    private int                       mOffsetRequest;
    // requested count
    private int                       mCountRequest;
    // got offset
    private int                       mOffsetResponse;
    // got count
    private int                       mCountResponse;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mLastResultSize = NO_RESULTS_YET;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        cancel();
    }

    private void load()
    {
        // if we always have required data
        if(mMelodies != null && mMelodies.size() >= mOffsetRequest + mCountRequest)
        {
            // return it with set to true flag of exists new data
            mTaskCallbacks.onLoad(mMelodies.size(), null);
        }
        else if(canLoadMoreData())
        {
            // start get new data
            if(!mRunning)
            {

                Retrofit retrofit = new Retrofit.Builder().baseUrl(Consts.API_BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();

                // prepare call in Retrofit 2.0
                IntechMelodyApi stackOverflowAPI = retrofit.create(IntechMelodyApi.class);

                Call<Melodies> call = stackOverflowAPI.loadMelodies(mOffsetRequest, mCountRequest);
                // asynchronous call
                call.enqueue(this);

                mRunning = true;
            }
        }
        else
        {
            // no more data
            mTaskCallbacks.onLoad(0, null);
        }
    }

    public void cancel()
    {
        if(mRunning)
        {
            //// TODO: 19.02.2016
            mRunning = false;
        }
    }

    public boolean isRunning()
    {
        return mRunning;
    }

    /* Retrofit Callback implementation */

    @Override
    public void onResponse(Call<Melodies> call, Response<Melodies> response)
    {
        Melodies melodies = response.body();

        if(melodies != null)
        {
            mLastResultSize = melodies.size();

            if(mLastResultSize > 0)
            {
                storeResult(melodies);
                mTaskCallbacks.onLoad(mLastResultSize, null);
            }
            else
            {
                mTaskCallbacks.onLoad(mLastResultSize, null);
            }
        }
        else
        {
            mLastResultSize = 0;

            // I do not know how it may happens, but I need be prepared to it

            mTaskCallbacks.onLoad(mLastResultSize, new Throwable("Null response"));
        }

        mRunning = false;
    }

    private void storeResult(@NonNull Melodies melodies)
    {
        if(mMelodies == null)
        {
            // either new instance...
            mMelodies = melodies;
        }
        else
        {
            // ...or add items
            mMelodies.add(melodies);
        }
    }

    @Override
    public void onFailure(Call<Melodies> call, Throwable t)
    {
        Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();

        mRunning = false;
    }

    /* IDataProvider implementation */

    @Override
    public void load(int offset, int count, @NonNull IDataHelper.TaskCallbacks taskCallbacks)
    {
        // store query
        mOffsetRequest = offset;
        mCountRequest = count;

        mTaskCallbacks = taskCallbacks;

        load();
    }

    @Override
    public boolean canLoadMoreData()
    {
        return mLastResultSize != 0;
    }

    @Override
    public int size()
    {
        return mMelodies != null ? mMelodies.size() : 0;
    }

    @Override
    public Melody get(int position)
    {
        return mMelodies != null ? mMelodies.get(position) : null;
    }
}
