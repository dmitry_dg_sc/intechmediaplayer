package com.intechglobal.exam.android.mediaplayer.ui.fragment;

/**
 * Created by DG on 19.02.2016.
 */
public interface IFragmentController
{
    void setTitle(String title);

    void showBackButton(boolean show);
}
