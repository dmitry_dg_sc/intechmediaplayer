package com.intechglobal.exam.android.mediaplayer.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;

/**
 * Created by DG on 18.02.2016.
 */
public class UiUtils
{
    public static boolean isLandscape(Context context)
    {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public static Drawable getDrawable(Context context, @DrawableRes int resId)
    {
        Resources resources = context.getResources();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            return resources.getDrawable(resId, context.getTheme());
        }
        else
        {
            //noinspection deprecation
            return resources.getDrawable(resId);
        }
    }
}
