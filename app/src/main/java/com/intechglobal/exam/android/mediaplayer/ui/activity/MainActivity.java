package com.intechglobal.exam.android.mediaplayer.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.intechglobal.exam.android.mediaplayer.R;
import com.intechglobal.exam.android.mediaplayer.api.model.Melody;
import com.intechglobal.exam.android.mediaplayer.media.Consts;
import com.intechglobal.exam.android.mediaplayer.provider.IDataHelper;
import com.intechglobal.exam.android.mediaplayer.provider.TaskRetainFragment;
import com.intechglobal.exam.android.mediaplayer.ui.fragment.IFragmentController;
import com.intechglobal.exam.android.mediaplayer.ui.fragment.MelodiesListFragment;
import com.intechglobal.exam.android.mediaplayer.ui.fragment.PlayerFragment;
import com.squareup.picasso.Picasso;

/**
 * Тестовое задание Intech – Android
 * <p/>
 * Имеется API (https://api-content-beeline.intech-global.com/public/marketplaces/1/tags/4/melodies),
 * возвращающий список песен. Необходимо разработать
 * приложение, получающее их от API, отображающее в таблице и проигрывающее каждый
 * Для каждой песни необходимо отобразить обложку, название и артиста. Для пагинации
 * к запросу добавляются параметры limit и from (пример: .../melodies?limit=10&from=0).
 * При первом запросе нужно получить 20 песен, затем подгружать следующие по
 * необходимости при прокрутке.
 * При нажатии на артиста из списка должен открываться экран с плеером. Плеер содержит в
 * себе обложку артиста, кнопку старт, пауза, стоп и скролл бар.
 * <p/>
 * Приложение должно поддерживать Android 4+
 * <p/>
 * Также, приложение должно:
 * 1. Поддерживать поворот экрана (в вертикальном положении 2 колонки в таблице,в горизонтальном 3)
 * 2. Иметь переключатель между таблицей и списком (необходимо использовать фрагменты)
 * <p/>
 * Разрешается использовать opensource решения.
 * <p/>
 * <p/>
 * all comments will be in english
 */
public class MainActivity extends AppCompatActivity implements IDataHelper, IFragmentController
{
    private static final String TAG = MainActivity.class.getSimpleName();

    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            showPlayerFragment((Melody) intent.getParcelableExtra(Consts.ARG_MELODY));
        }
    };

    private Picasso mPicasso;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // first run - show melodies list
        if(savedInstanceState == null)
        {
            /** pre init.
             * I have a case, when firstly called {@link IDataProvider#load(int, int, TaskCallbacks)}
             * before then {@link TaskRetainFragment#onCreate(Bundle)}
             * and {@link TaskRetainFragment#mLastResultSize} was not initialized */
            getDataProvider();
            showMelodiesListFragment();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        IntentFilter filter = new IntentFilter(Consts.ACTION_OPEN_PLAYER);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
    }

    @Override
    public void onPause()
    {
        super.onPause();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        switch(id)
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * melodies list
     */
    private void showMelodiesListFragment()
    {
        final String tag = MelodiesListFragment.TAG;

        // will use support because android.app.FragmentManager worse than android.support.v4.app.FragmentManager
        // and wrong works with backStack
        FragmentManager fm = getSupportFragmentManager();
        MelodiesListFragment melodiesListFragment = (MelodiesListFragment) fm.findFragmentByTag(tag);

        if(melodiesListFragment == null)
        {
            melodiesListFragment = MelodiesListFragment.newInstance();
        }

        FragmentTransaction ft = fm.beginTransaction();

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.container, melodiesListFragment, tag);
        // is root fragment ft.addToBackStack(tag);
        ft.commit();

        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    private void showPlayerFragment(@NonNull Melody melody)
    {
        final String tag = PlayerFragment.TAG;

        FragmentManager fm = getSupportFragmentManager();
        PlayerFragment playerFragment = (PlayerFragment) fm.findFragmentByTag(tag);

        if(playerFragment == null)
        {
            playerFragment = PlayerFragment.newInstance(melody);
        }

        FragmentTransaction ft = fm.beginTransaction();

        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.container, playerFragment, tag);
        ft.addToBackStack(tag);
        ft.commit();
    }

    /**
     * Returns a Data Provider.
     * Now we use RetainFragment with stored response data
     *
     * @return
     * @see TaskRetainFragment#mMelodies
     */
    @NonNull
    @Override
    public IDataProvider getDataProvider()
    {
        FragmentManager fm = getSupportFragmentManager();
        TaskRetainFragment taskRetainFragment = (TaskRetainFragment) fm.findFragmentByTag(TaskRetainFragment.TAG);

        if(taskRetainFragment == null)
        {
            taskRetainFragment = new TaskRetainFragment();
            fm.beginTransaction().add(taskRetainFragment, TaskRetainFragment.TAG).commit();
        }

        return taskRetainFragment;
    }

    @NonNull
    @Override
    public Picasso getPicasso()
    {
        if(mPicasso == null)
        {
            mPicasso = new Picasso.Builder(this).defaultBitmapConfig(Bitmap.Config.RGB_565).build();
        }

        return mPicasso;
    }

    @Override
    public void setTitle(String title)
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public void showBackButton(boolean show)
    {
        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayHomeAsUpEnabled(show);
        }
    }
}
