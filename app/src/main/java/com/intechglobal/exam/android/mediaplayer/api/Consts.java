package com.intechglobal.exam.android.mediaplayer.api;

/**
 * Created by DG on 19.02.2016.
 */
public interface Consts
{
    /**
     *  without "/melodies"
     *  @see IntechMelodyApi#loadMelodies
     */
    String API_BASE_URL = "https://api-content-beeline.intech-global.com/";

}
