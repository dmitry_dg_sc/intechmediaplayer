package com.intechglobal.exam.android.mediaplayer.provider;

import android.support.annotation.NonNull;

import com.intechglobal.exam.android.mediaplayer.api.model.Melodies;
import com.squareup.picasso.Picasso;

/**
 * Melodies provider holder (ex.Activity with retain fragment)
 * Picasso
 */
public interface IDataHelper
{

    interface TaskCallbacks
    {
        /**
         *
         * @param hasNewData true if next data was got, false if rich end of data
         * @param error
         */
//        void onLoad(boolean hasNewData, Throwable error);

        void onLoad(int lastResponseSize, Throwable error);
    }

    /**
     * Melodies provider interface
     */
    interface IDataProvider<D>
    {
        /**
         * Loads {@link Melodies} from source (web, ...)
         * @param offset from
         * @param count  count
         * @return
         */
        void load(int offset, int count, TaskCallbacks taskCallbacks);

        boolean canLoadMoreData();

        int size();

        D get(int position);
    }

    @NonNull
    IDataProvider getDataProvider();

    /**
     * Must I create another interface? (to SOLID maniacs question)
     * @return
     */
    @NonNull
    Picasso getPicasso();
}
