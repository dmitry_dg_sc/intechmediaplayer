package com.intechglobal.exam.android.mediaplayer.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intechglobal.exam.android.mediaplayer.R;
import com.intechglobal.exam.android.mediaplayer.api.model.Melody;
import com.intechglobal.exam.android.mediaplayer.provider.IDataHelper;
import com.intechglobal.exam.android.mediaplayer.util.UiUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by DG on 18.02.2016.
 */
public class MelodiesAdapter extends RecyclerView.Adapter<AbsractViewHolder> implements View.OnClickListener
{
    public interface OnItemClickListener
    {
        void onItemClicked(@Nullable Melody melody);
    }

    public interface OnLoadMoreRequestListener
    {
        void onLoadMoreRequested();
    }

    public static class FooterViewHolder extends AbsractViewHolder
    {
        public FooterViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }

        @Override
        public void bindData(@NonNull Melody melody)
        {
            // skip
        }
    }

    public static class MelodyViewHolder extends AbsractViewHolder implements Target
    {
        public final ImageView albumImage;
        public final TextView  title;
        public final TextView  artist;

        public MelodyViewHolder(@NonNull View itemView)
        {
            super(itemView);

            albumImage = (ImageView) itemView.findViewById(R.id.album_image);
            title = (TextView) itemView.findViewById(R.id.title);
            artist = (TextView) itemView.findViewById(R.id.artist);
        }

        @Override
        public void bindData(@NonNull Melody melody)
        {
            sPicasso.load(Uri.parse(melody.getPicUrl())).resize(150, 150).centerInside().error(sErrorDrawable).into(this);

            title.setText(melody.getTitle());
            artist.setText(melody.getArtist());
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
        {
            albumImage.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable)
        {
            //albumImage.setImageDrawable(errorDrawable);
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable)
        {
            // // TODO: 18.02.2016 add progress
        }
    }

    private static final int TYPE_GRID_VIEW   = 0;
    private static final int TYPE_ROW_VIEW    = 1;
    private static final int TYPE_FOOTER_VIEW = 2;

    private static Picasso                           sPicasso;
    private static Drawable                          sErrorDrawable;
    private final  LayoutInflater                    mLayoutInflater;
    private final  OnItemClickListener               mOnItemClickListener;
    private final  OnLoadMoreRequestListener         mOnLoadMoreRequestListener;
    private        boolean                           isGridView;
    private        IDataHelper.IDataProvider<Melody> mMelodiesDataProvider;

    public MelodiesAdapter(@NonNull Context context, @NonNull IDataHelper.IDataProvider<Melody> melodiesDataProvider, @NonNull Picasso picasso, @NonNull OnItemClickListener onItemClickListener, OnLoadMoreRequestListener onLoadMoreRequestListener)
    {
        mLayoutInflater = LayoutInflater.from(context);
        mMelodiesDataProvider = melodiesDataProvider;
        sPicasso = picasso;
        mOnItemClickListener = onItemClickListener;
        mOnLoadMoreRequestListener = onLoadMoreRequestListener;

        sErrorDrawable = UiUtils.getDrawable(context, R.drawable.stub_image);
    }

    @Override
    public AbsractViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view;
        if(viewType == TYPE_FOOTER_VIEW)
        {
            view = mLayoutInflater.inflate(R.layout.list_footer, parent, false);

            return new FooterViewHolder(view);
        }
        else
        {
            view = isGridView ? mLayoutInflater.inflate(R.layout.list_item_melody_grid, parent, false) : mLayoutInflater.inflate(R.layout.list_item_melody_single_row, parent, false);
            view.setOnClickListener(this);

            return new MelodyViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(AbsractViewHolder holder, int position)
    {
        Melody melody = mMelodiesDataProvider.get(position);
        if(melody != null)
        {
            holder.bindData(melody);

            checkAndDispatchLoadMoreEvent(position);
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        return position >= mMelodiesDataProvider.size() ? TYPE_FOOTER_VIEW : isGridView ? TYPE_GRID_VIEW : TYPE_ROW_VIEW;
    }

    @Override
    public int getItemCount()
    {
        int dataSize = mMelodiesDataProvider.size();
        if(mMelodiesDataProvider.canLoadMoreData())
        {
            // allow show footer view
            return dataSize + 1;
        }
        else
        {
            return mMelodiesDataProvider.size();
        }
    }

    @Override
    public void onClick(View view)
    {
        final RecyclerView recyclerView = (RecyclerView) view.getParent();
        final int position = recyclerView.getChildLayoutPosition(view);
        if(position != RecyclerView.NO_POSITION)
        {
            final Melody melody = getItem(position);
            mOnItemClickListener.onItemClicked(melody);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Nullable
    private Melody getItem(int position)
    {
        return mMelodiesDataProvider.get(position);
    }

    public void setGridView(boolean gridView)
    {
        if(isGridView != gridView)
        {
            isGridView = gridView;

            notifyDataSetChanged();
        }
    }

    private void checkAndDispatchLoadMoreEvent(int position)
    {
        if(mMelodiesDataProvider.canLoadMoreData() && position == mMelodiesDataProvider.size() - 1 && mOnLoadMoreRequestListener != null)
        {
            mOnLoadMoreRequestListener.onLoadMoreRequested();
        }
    }
}
