package com.intechglobal.exam.android.mediaplayer.api.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DG on 18.02.2016.
 * @see Melody
 */
public class Tag implements Parcelable
{
    private int     id;
    private String  title;
    private boolean limitedVisibility;
    private int     position;
    private boolean isFullCatalogEnabled;
    private int     topMelodiesCount;
    private boolean isBlockDisplayMode;

    public Tag()
    {

    }

    protected Tag(Parcel in)
    {
        id = in.readInt();
        title = in.readString();
        limitedVisibility = in.readByte() != 0;
        position = in.readInt();
        isFullCatalogEnabled = in.readByte() != 0;
        topMelodiesCount = in.readInt();
        isBlockDisplayMode = in.readByte() != 0;
    }

    public static final Creator<Tag> CREATOR = new Creator<Tag>()
    {
        @Override
        public Tag createFromParcel(Parcel in)
        {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size)
        {
            return new Tag[size];
        }
    };

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public boolean isLimitedVisibility()
    {
        return limitedVisibility;
    }

    public void setLimitedVisibility(boolean limitedVisibility)
    {
        this.limitedVisibility = limitedVisibility;
    }

    public int getPosition()
    {
        return position;
    }

    public void setPosition(int position)
    {
        this.position = position;
    }

    public boolean isFullCatalogEnabled()
    {
        return isFullCatalogEnabled;
    }

    public void setIsFullCatalogEnabled(boolean isFullCatalogEnabled)
    {
        this.isFullCatalogEnabled = isFullCatalogEnabled;
    }

    public int getTopMelodiesCount()
    {
        return topMelodiesCount;
    }

    public void setTopMelodiesCount(int topMelodiesCount)
    {
        this.topMelodiesCount = topMelodiesCount;
    }

    public boolean isBlockDisplayMode()
    {
        return isBlockDisplayMode;
    }

    public void setIsBlockDisplayMode(boolean isBlockDisplayMode)
    {
        this.isBlockDisplayMode = isBlockDisplayMode;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeByte((byte) (limitedVisibility ? 1 : 0));
        dest.writeInt(position);
        dest.writeByte((byte) (isFullCatalogEnabled ? 1 : 0));
        dest.writeInt(topMelodiesCount);
        dest.writeByte((byte) (isBlockDisplayMode ? 1 : 0));
    }
}
