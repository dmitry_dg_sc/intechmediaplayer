package com.intechglobal.exam.android.mediaplayer.media;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by DG on 19.02.2016.
 */
public class InternalMediaPlayer extends MediaPlayer implements MediaPlayer.OnPreparedListener
{
    private boolean            mIsPrepared;
    private OnPreparedListener mOnPreparedListener;

    public void setDataSource(String path)
    {
        try
        {
            super.reset();
            super.setDataSource(path);
            super.setOnPreparedListener(this);
            super.prepareAsync();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public boolean isPrepared()
    {
        return mIsPrepared;
    }

    @Override
    public void stop()
    {
        super.reset();
        mIsPrepared = false;
    }

    @Override
    public void release()
    {
        stop();
        super.release();
    }

    @Override
    public void onPrepared(MediaPlayer mp)
    {
        mIsPrepared = true;

        // update controls view
        if(mOnPreparedListener != null)
        {
            mOnPreparedListener.onPrepared(this);
        }
    }

    @Override
    public void setOnPreparedListener(OnPreparedListener onPreparedListener)
    {
        this.mOnPreparedListener = onPreparedListener;
    }
}
