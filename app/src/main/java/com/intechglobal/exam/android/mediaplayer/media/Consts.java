package com.intechglobal.exam.android.mediaplayer.media;

import com.intechglobal.exam.android.mediaplayer.service.MediaPlaybackService;

/**
 * Created by DG on 19.02.2016.
 */
public interface Consts
{
    /**
     *  Action for broadcast message. Open media player
     *  @see Consts#ARG_MELODY
     */
    String ACTION_OPEN_PLAYER  = "com.intechglobal.exam.android.mediaplayer.media.open";
    /**
     * Action for broadcast message. Error in media player
     *
     * @see MediaPlaybackService#onCreate()
     * @see android.media.MediaPlayer.OnErrorListener
     * @see Consts#ARG_ERROR_WHAT
     * @see Consts#ARG_ERROR_EXTRA
     */
    String ACTION_PLAYER_ERROR = "com.intechglobal.exam.android.mediaplayer.media.error";
    /**
     * Action for broadcast message. The media player is prepared
     *
     * @see MediaPlaybackService#onCreate()
     * @see android.media.MediaPlayer.OnPreparedListener
     * @see Consts#ARG_STATE_KEY
     * @see Consts#ARG_STATE_PREPARED
     */
    String ACTION_PLAYER_STATE = "com.intechglobal.exam.android.mediaplayer.media.state";

    /**
     *  An argument to store {@link com.intechglobal.exam.android.mediaplayer.api.model.Melody}
     *  in intent to open media player
     */
    String ARG_MELODY = "melody";

    /**
     * Broadcast extra fields for {@link android.media.MediaPlayer.OnErrorListener}
     */
    String ARG_ERROR_WHAT     = "what";
    String ARG_ERROR_EXTRA    = "extra";
    /**
     * Broadcast extra fields
     * States of MediaPlayer
     */
    String ARG_STATE_KEY      = "state";
    String ARG_STATE_PREPARED = "prepared";
    String ARG_STATE_PLAY     = "play";
    String ARG_STATE_PAUSED   = "paused";
    String ARG_STATE_STOPPED  = "stopped";
    String ARG_STATE_COMPLETE = "complete";
    String ARG_STATE_UNKNOWN  = "unknown";
}
