package com.intechglobal.exam.android.mediaplayer.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.intechglobal.exam.android.mediaplayer.R;
import com.intechglobal.exam.android.mediaplayer.api.model.Melody;
import com.intechglobal.exam.android.mediaplayer.media.Consts;
import com.intechglobal.exam.android.mediaplayer.provider.IDataHelper;
import com.intechglobal.exam.android.mediaplayer.service.IMediaPlaybackInterface;
import com.intechglobal.exam.android.mediaplayer.service.MediaPlaybackService;
import com.intechglobal.exam.android.mediaplayer.util.TimeUtils;
import com.intechglobal.exam.android.mediaplayer.util.UiUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by DG on 19.02.2016.
 */
public class PlayerFragment extends Fragment
{
    public static final String TAG = PlayerFragment.class.getSimpleName();

    private static final String ARG_MELODY = "melody";

    /**
     * Receiver for broadcast message from {@link MediaPlaybackService}
     */
    private final BroadcastReceiver mReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();

            if(action.equals(Consts.ACTION_PLAYER_ERROR))
            {
                int what = intent.getIntExtra(Consts.ARG_ERROR_WHAT, 0);
                int extra = intent.getIntExtra(Consts.ARG_ERROR_EXTRA, 0);

                Toast.makeText(getContext(), String.format(getResources().getString(R.string.error_media_player), what, extra), Toast.LENGTH_LONG).show();
            }
            else if(action.equals(Consts.ACTION_PLAYER_STATE))
            {
                mState = intent.getStringExtra(Consts.ARG_STATE_KEY);

                processState();
            }
        }
    };

    private final ServiceConnection mServiceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            mService = IMediaPlaybackInterface.Stub.asInterface(service);
            mIsBound = true;

            mProgressBar.setVisibility(View.VISIBLE);
            mSeekBar.setVisibility(View.GONE);
            try
            {
                mService.openFile(mMelody.getDemoUrl());
            }
            catch(Throwable e)
            {
                Toast.makeText(getContext(), String.format(getResources().getString(R.string.error_open_media_file), e.getLocalizedMessage()), Toast.LENGTH_LONG).show();

                unbindService();

                getActivity().onBackPressed();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName classname)
        {
            mService = null;
            mIsBound = false;
        }
    };

    private Handler mHandler = new Handler();


    private IMediaPlaybackInterface mService;
    private boolean                 mIsBound;
    private Melody                  mMelody;
    private Picasso                 mPicasso;
    private ImageView               mImageView;
    private TextView                mCurrentTime;
    private TextView                mTotalTime;
    private ProgressBar             mProgressBar;
    private Button                  mPlay;
    private Button                  mPause;
    private Button                  mStop;
    private SeekBar                 mSeekBar;
    private String                  mState;
    private IFragmentController     mFragmentController;

    public static PlayerFragment newInstance(@NonNull Melody melody)
    {
        PlayerFragment fragment = new PlayerFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_MELODY, melody);
        fragment.setArguments(args);
        return fragment;
    }

    public PlayerFragment()
    {
        // in onResume - bindService
        // in onDestroy - unbindService
        // and configuration changes have not any effect to service
        setRetainInstance(true);

        mState = Consts.ARG_STATE_UNKNOWN;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        if(context instanceof IDataHelper)
        {
            mPicasso = ((IDataHelper) context).getPicasso();
        }
        else
        {
            throw new IllegalArgumentException(context.toString() + " must implement " + IDataHelper.class.getSimpleName());
        }

        if(context instanceof IFragmentController)
        {
            mFragmentController = (IFragmentController) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_player, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        final Context context = view.getContext();

        mImageView = (ImageView) view.findViewById(R.id.album_image);
        mCurrentTime = (TextView) view.findViewById(R.id.current_time);
        mTotalTime = (TextView) view.findViewById(R.id.total_time);
        mSeekBar = (SeekBar) view.findViewById(R.id.seek_bar);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                try
                {
                    if(fromUser && mService != null && mService.isPlaying())
                    {
                        mService.seek(progress * 1000);
                        mCurrentTime.setText(TimeUtils.TIME_FORMAT.format(progress * 1000));
                    }
                }
                catch(RemoteException e)
                {

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar)
            {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar)
            {
            }
        });
        mProgressBar = (ProgressBar) view.findViewById(R.id.buffering_progress_bar);
        mPlay = (Button) view.findViewById(R.id.play);
        mPlay.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mService != null)
                {
                    try
                    {
                        mService.play();
                    }
                    catch(RemoteException e)
                    {

                    }
                }
            }
        });

        mPause = (Button) view.findViewById(R.id.pause);
        mPause.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mService != null)
                {
                    try
                    {
                        mService.pause();
                    }
                    catch(RemoteException e)
                    {

                    }
                }
            }
        });

        mStop = (Button) view.findViewById(R.id.stop);
        mStop.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(mService != null)
                {
                    try
                    {
                        mService.stop();
                    }
                    catch(RemoteException e)
                    {

                    }
                }
            }
        });

        mMelody = getArguments().getParcelable(ARG_MELODY);

        if(mMelody != null)
        {
            // invalidate resized image
            mPicasso.invalidate(mMelody.getPicUrl());
            mPicasso.load(mMelody.getPicUrl()).error(UiUtils.getDrawable(context, R.drawable.stub_image)).into(mImageView);
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // need reinit if screen orientation changed
        initializeSeekBar();

        // update buttons visibility
        processState();

        if(mFragmentController != null)
        {
            mFragmentController.setTitle(getResources().getString(R.string.player_title));
            // child fragment - need back button
            mFragmentController.showBackButton(true);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(Consts.ACTION_PLAYER_ERROR);
        filter.addAction(Consts.ACTION_PLAYER_STATE);
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mReceiver, filter);

        // при показе подключимся
        if(!mIsBound)
        {
            bindService();
        }
    }

    @Override
    public void onDestroy()
    {
        // при повороте экрана отключимся
        if(mIsBound)
        {
            unbindService();
        }

        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiver);

        mHandler.removeCallbacksAndMessages(null);

        if(mMelody != null)
        {
            // invalidate original image
            mPicasso.invalidate(mMelody.getPicUrl());
        }

        super.onDestroy();
    }

    private void bindService()
    {
        getActivity().getApplicationContext().bindService(new Intent(getContext(), MediaPlaybackService.class), mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void unbindService()
    {
        getActivity().getApplicationContext().unbindService(mServiceConnection);
        mIsBound = false;
    }

    private void processState()
    {
        switch(mState)
        {
            case Consts.ARG_STATE_PREPARED:
                mProgressBar.setVisibility(View.GONE);
                mSeekBar.setVisibility(View.VISIBLE);

                mPlay.setEnabled(true);
                mPause.setEnabled(false);
                mStop.setEnabled(false);

                initializeSeekBar();

                mCurrentTime.setText(TimeUtils.TIME_FORMAT.format(0));
                break;

            case Consts.ARG_STATE_PLAY:
                mPlay.setEnabled(false);
                mPause.setEnabled(true);
                mStop.setEnabled(true);

                // update seekbar progress
                updateSeekBar(true);

                break;

            case Consts.ARG_STATE_PAUSED:
                mPlay.setEnabled(true);
                mPause.setEnabled(false);
                mStop.setEnabled(true);

                // stop update
                mHandler.removeCallbacksAndMessages(null);
                break;

            case Consts.ARG_STATE_STOPPED:
                mPlay.setEnabled(false);
                mPause.setEnabled(false);
                mStop.setEnabled(false);

                // stop update
                mHandler.removeCallbacksAndMessages(null);
                break;

            case Consts.ARG_STATE_COMPLETE:
                mPlay.setEnabled(true);
                mPause.setEnabled(false);
                mStop.setEnabled(false);

                // final update seekbar progress
                updateSeekBar(false);
                // stop update
                mHandler.removeCallbacksAndMessages(null);

                break;

            default:
                mProgressBar.setVisibility(View.GONE);

                mPlay.setEnabled(false);
                mPause.setEnabled(false);
                mStop.setEnabled(false);

                // stop update
                mHandler.removeCallbacksAndMessages(null);
                break;
        }
    }

    private void initializeSeekBar()
    {
        getActivity().runOnUiThread(new Runnable()
                                    {

                                        @Override
                                        public void run()
                                        {
                                            long total = 0;
                                            try
                                            {
                                                if(mService != null)
                                                {
                                                    total = mService.getDuration();
                                                }
                                            }
                                            catch(Exception e)
                                            {

                                            }
                                            mSeekBar.setMax((int) (total / 1000));

                                            mTotalTime.setText(TimeUtils.TIME_FORMAT.format(total));
                                            //mCurrentTime.setText(TimeUtils.TIME_FORMAT.format(0));
                                        }
                                    }

        );
    }

    private void updateSeekBar(final boolean repeat)
    {
        getActivity().runOnUiThread(new Runnable()
                                    {

                                        @Override
                                        public void run()
                                        {
                                            long currentPosition = 0;
                                            try
                                            {
                                                currentPosition = mService.getPosition();
                                            }
                                            catch(RemoteException e)
                                            {

                                            }

                                            mSeekBar.setProgress((int) (currentPosition / 1000));
                                            mCurrentTime.setText(TimeUtils.TIME_FORMAT.format(currentPosition));

                                            if(repeat)
                                            {
                                                mHandler.postDelayed(this, 1000);
                                            }
                                        }
                                    }

        );
    }
}
