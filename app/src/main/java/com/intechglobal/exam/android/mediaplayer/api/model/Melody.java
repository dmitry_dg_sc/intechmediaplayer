package com.intechglobal.exam.android.mediaplayer.api.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by DG on 18.02.2016.
 * @see Melodies
 * @see Tag
 */
public class Melody implements Parcelable
{
    private int            id;
    private String         title;
    private int            artistId;
    private String         artist;
    private String         code;
    private String         smsNumber;
    private String         price;
    private int            fPrice;
    private String         prolongationPrice;
    private int            fProlongationPrice;
    private String         purchasePeriod;
    private int            position;
    private String         picUrl;
    private String         demoUrl;
    private int            intechMelodyId;
    private ArrayList<Tag> tags;
    private boolean        active;
    private int            relevance;
    private String         melodyId;

    public Melody()
    {

    }

    protected Melody(Parcel in)
    {
        id = in.readInt();
        title = in.readString();
        artistId = in.readInt();
        artist = in.readString();
        code = in.readString();
        smsNumber = in.readString();
        price = in.readString();
        fPrice = in.readInt();
        prolongationPrice = in.readString();
        fProlongationPrice = in.readInt();
        purchasePeriod = in.readString();
        position = in.readInt();
        picUrl = in.readString();
        demoUrl = in.readString();
        intechMelodyId = in.readInt();
        tags = in.createTypedArrayList(Tag.CREATOR);
        active = in.readByte() != 0;
        relevance = in.readInt();
        melodyId = in.readString();
    }

    public static final Creator<Melody> CREATOR = new Creator<Melody>()
    {
        @Override
        public Melody createFromParcel(Parcel in)
        {
            return new Melody(in);
        }

        @Override
        public Melody[] newArray(int size)
        {
            return new Melody[size];
        }
    };

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getArtistId()
    {
        return artistId;
    }

    public void setArtistId(int artistId)
    {
        this.artistId = artistId;
    }

    public String getArtist()
    {
        return artist;
    }

    public void setArtist(String artist)
    {
        this.artist = artist;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getSmsNumber()
    {
        return smsNumber;
    }

    public void setSmsNumber(String smsNumber)
    {
        this.smsNumber = smsNumber;
    }

    public String getPrice()
    {
        return price;
    }

    public void setPrice(String price)
    {
        this.price = price;
    }

    public int getfPrice()
    {
        return fPrice;
    }

    public void setfPrice(int fPrice)
    {
        this.fPrice = fPrice;
    }

    public String getProlongationPrice()
    {
        return prolongationPrice;
    }

    public void setProlongationPrice(String prolongationPrice)
    {
        this.prolongationPrice = prolongationPrice;
    }

    public int getfProlongationPrice()
    {
        return fProlongationPrice;
    }

    public void setfProlongationPrice(int fProlongationPrice)
    {
        this.fProlongationPrice = fProlongationPrice;
    }

    public String getPurchasePeriod()
    {
        return purchasePeriod;
    }

    public void setPurchasePeriod(String purchasePeriod)
    {
        this.purchasePeriod = purchasePeriod;
    }

    public int getPosition()
    {
        return position;
    }

    public void setPosition(int position)
    {
        this.position = position;
    }

    public String getPicUrl()
    {
        return picUrl;
    }

    public void setPicUrl(String picUrl)
    {
        this.picUrl = picUrl;
    }

    public String getDemoUrl()
    {
        return demoUrl;
    }

    public void setDemoUrl(String demoUrl)
    {
        this.demoUrl = demoUrl;
    }

    public int getIntechMelodyId()
    {
        return intechMelodyId;
    }

    public void setIntechMelodyId(int intechMelodyId)
    {
        this.intechMelodyId = intechMelodyId;
    }

    public ArrayList<Tag> getTags()
    {
        return tags;
    }

    public void setTags(ArrayList<Tag> tags)
    {
        this.tags = tags;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public int getRelevance()
    {
        return relevance;
    }

    public void setRelevance(int relevance)
    {
        this.relevance = relevance;
    }

    public String getMelodyId()
    {
        return melodyId;
    }

    public void setMelodyId(String melodyId)
    {
        this.melodyId = melodyId;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeInt(artistId);
        dest.writeString(artist);
        dest.writeString(code);
        dest.writeString(smsNumber);
        dest.writeString(price);
        dest.writeInt(fPrice);
        dest.writeString(prolongationPrice);
        dest.writeInt(fProlongationPrice);
        dest.writeString(purchasePeriod);
        dest.writeInt(position);
        dest.writeString(picUrl);
        dest.writeString(demoUrl);
        dest.writeInt(intechMelodyId);
        dest.writeTypedList(tags);
        dest.writeByte((byte) (active ? 1 : 0));
        dest.writeInt(relevance);
        dest.writeString(melodyId);
    }
}
