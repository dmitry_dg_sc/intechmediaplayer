package com.intechglobal.exam.android.mediaplayer.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.intechglobal.exam.android.mediaplayer.api.model.Melody;

/**
 * Created by DG on 19.02.2016.
 */
public abstract class AbsractViewHolder extends RecyclerView.ViewHolder
{
    public AbsractViewHolder(View itemView)
    {
        super(itemView);
    }

    abstract void bindData(@NonNull Melody melody);
}
