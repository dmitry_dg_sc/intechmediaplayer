package com.intechglobal.exam.android.mediaplayer.api;

import com.intechglobal.exam.android.mediaplayer.api.model.Melodies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by DG on 19.02.2016.
 */
public interface IntechMelodyApi
{
    @GET("public/marketplaces/1/tags/4/melodies")
    Call<Melodies> loadMelodies(@Query("limit") int limit, @Query("from") int from);
}
