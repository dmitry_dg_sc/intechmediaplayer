package com.intechglobal.exam.android.mediaplayer.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.intechglobal.exam.android.mediaplayer.R;
import com.intechglobal.exam.android.mediaplayer.api.model.Melody;
import com.intechglobal.exam.android.mediaplayer.media.Consts;
import com.intechglobal.exam.android.mediaplayer.provider.IDataHelper;
import com.intechglobal.exam.android.mediaplayer.ui.adapter.MelodiesAdapter;
import com.intechglobal.exam.android.mediaplayer.util.UiUtils;
import com.squareup.picasso.Picasso;

/**
 * Created by DG on 18.02.2016.
 */
public class MelodiesListFragment extends Fragment implements MelodiesAdapter.OnItemClickListener, IDataHelper.TaskCallbacks, MelodiesAdapter.OnLoadMoreRequestListener
{
    public static final String TAG = MelodiesListFragment.class.getSimpleName();

    private static final String ARG_COLUMN_COUNT = "column_count";
    private static final String ARG_OFFSET       = "offset";
    private static final String ARG_COUNT        = "count";

    private static final int MAX_LANDSCAPE_COLUMN = 3;
    private static final int MAX_PORTRAIT_COLUMN  = 2;

    private static final int FIRST_OFFSET  = 0;
    private static final int DEFAULT_COUNT = 20;

    private RecyclerView                      mRecyclerView;
    private MelodiesAdapter                   mMelodiesAdapter;
    private int                               mColumnCount;
    private int                               mOffset;
    private int                               mCount;
    private IDataHelper.IDataProvider<Melody> mDataProvider;
    private Picasso                           mPicasso;
    private IFragmentController               mFragmentController;

    public static MelodiesListFragment newInstance()
    {
        MelodiesListFragment fragment = new MelodiesListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public MelodiesListFragment()
    {
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        if(context instanceof IDataHelper)
        {
            // noinspection unchecked
            mDataProvider = ((IDataHelper) context).getDataProvider();
            mPicasso = ((IDataHelper) context).getPicasso();
        }
        else
        {
            throw new IllegalArgumentException(context.toString() + " must implement " + IDataHelper.class.getSimpleName());
        }

        if(context instanceof IFragmentController)
        {
            mFragmentController = (IFragmentController) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        final Context context = view.getContext();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        if(mMelodiesAdapter == null)
        {
            mMelodiesAdapter = new MelodiesAdapter(context, mDataProvider, mPicasso, this, this);
        }

        mRecyclerView.setAdapter(mMelodiesAdapter);

        if(savedInstanceState != null)
        {
            mColumnCount = savedInstanceState.getInt(ARG_COLUMN_COUNT);

            mOffset = savedInstanceState.getInt(ARG_OFFSET);
            mCount = savedInstanceState.getInt(ARG_COUNT);
        }
        else
        {
            mColumnCount = MAX_PORTRAIT_COLUMN;

            mOffset = FIRST_OFFSET;
            mCount = DEFAULT_COUNT;
        }

        validateColumnCount(context);

        setLayoutManager();
    }

    private void validateColumnCount(Context context)
    {
        if(mColumnCount == 1)
        {
            // staying as is independently on screen orientation
            return;
        }

        if(UiUtils.isLandscape(context))
        {
            mColumnCount = MAX_LANDSCAPE_COLUMN;
        }
        else
        {
            mColumnCount = MAX_PORTRAIT_COLUMN;
        }
    }

    private void setLayoutManager()
    {
        if(mColumnCount <= 1)
        {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        else
        {
            mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), mColumnCount));
        }

        mMelodiesAdapter.setGridView(mColumnCount > 1);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mFragmentController != null)
        {
            mFragmentController.setTitle(getResources().getString(R.string.melodies_list_title));
            // root fragment - no need back button
            mFragmentController.showBackButton(false);
        }

        // prepare initial data
        mDataProvider.load(mOffset, mCount, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        switch(id)
        {
            case R.id.action_change_grid:
                nextGridView();
                return true;

            default:
                return false;
        }
    }

    /**
     * Cyclic change linear view <-> grid view
     */
    private void nextGridView()
    {
        final int maxColumn;
        if(UiUtils.isLandscape(getActivity()))
        {
            maxColumn = MAX_LANDSCAPE_COLUMN;
        }
        else
        {
            maxColumn = MAX_PORTRAIT_COLUMN;
        }

        mColumnCount++;
        if(mColumnCount > maxColumn)
        {
            mColumnCount = 1;
        }

        validateColumnCount(getActivity());
        setLayoutManager();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt(ARG_COLUMN_COUNT, mColumnCount);
        outState.putInt(ARG_OFFSET, mOffset);
        outState.putInt(ARG_COUNT, mCount);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        mDataProvider = null;
    }

    @Override
    public void onItemClicked(@Nullable Melody melody)
    {
        Intent intent = new Intent(Consts.ACTION_OPEN_PLAYER);
        intent.putExtra(Consts.ARG_MELODY, melody);
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
    }

    @Override
    public void onLoad(int lastResponseSize, Throwable error)
    {
        if(error == null)
        {
            if(lastResponseSize > 0)
            {
                mMelodiesAdapter.notifyItemInserted(mOffset);
            }
            else
            {
                mMelodiesAdapter.notifyDataSetChanged();
            }
        }
        else
        {
            Toast.makeText(getActivity(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLoadMoreRequested()
    {
        mOffset = mDataProvider.size();
        mCount = DEFAULT_COUNT;

        mDataProvider.load(mOffset, mCount, this);
    }
}
